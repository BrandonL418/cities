//
//  DriverList.swift
//  Mate
//
//  Created by Landon Brambert on 9/16/19.
//  Copyright © 2019 Onyx Theaters.tv, INC. All rights reserved.
//

import Foundation
import HomeKit

class LasVegas: Drivers {
    init() {
        super.init(drivers: [
            //MARK: - Public Transportation
            
            Driver(id: "Transit Tracker", directLink: "tt.las://", safeLink: "https://apps.apple.com/us/app/transit-tracker-las-vegas/id989877024", type: .PublicTransportation, driverCategory: DriverCategory(id: .PublicTransportation, page: .travel, image: "")),
            Driver(id: "rideRTC", directLink: "rtcjustride://", safeLink: "https://apps.apple.com/us/app/ridertc/id909691507", type: .PublicTransportation, driverCategory: DriverCategory(id: .PublicTransportation, page: .travel, image: "")),
            Driver(id: "Las Vegas Transit", directLink: "", safeLink: "https://apps.apple.com/us/app/las-vegas-transit-rtc-times/id1176580994", type: .PublicTransportation, driverCategory: DriverCategory(id: .PublicTransportation, page: .travel, image: "")),
            
            
            
            
            
            
            
            //MARK: - RideShare
           
            Driver(id: "Citi Bike", directLink: "", safeLink: "https://apps.apple.com/us/app/citi-bike-miami/id398956248", type: .RideShare, driverCategory: DriverCategory(id: .RideShare, page: .travel, image: "")),
            
            //MARK: - Flights
        //    Driver(id: "Orlando MCO", directLink: "", safeLink: "https://apps.apple.com/us/app/orlando-mco/id939593688", type: .Flights, driverCategory: DriverCategory(id: .Flights, page: .weather, image: "")),
            
            //MARK: - Hotels
         //   Driver(id: "Orlando World Center Marriott", directLink: "mwcwayfinding", safeLink: "https://apps.apple.com/us/app/orlando-world-center-marriott/id1311856070", type: .Hotels, driverCategory: DriverCategory(id: .Hotels, page: .travel, image: "")),
            
           //MARK: - Calendars
         //   Driver(id: "VEGAS", directLink: "fb2050571868576748://", safeLink: "https://apps.apple.com/us/app/miami-travel-guide/id386132391", type: .Calendars, driverCategory: DriverCategory(id: .Calendars, page: .travel, image: "")),
            
           //MARK: - Restaraunts
           // Driver(id: "Hand-Picked", directLink: "scoutmobhp://", safeLink: "https://apps.apple.com/us/app/hand-picked-atlanta/id346700012", type: .Restaraunts, driverCategory: DriverCategory(id: .Restaraunts, page: .travel, image: "")),
            //MARK: - Mobile Food
           // Driver(id: "Uber eats", directLink: "ubereats", safeLink: "https://apps.apple.com/us/app/uber-eats-food-delivery/id1058959277", type: .MobileFood, driverCategory: DriverCategory(id: .MobileFood, page: .travel, image: "")),
            
            //MARK: - TicketedEvents
            
            Driver(id: "MLB At Bat", directLink: "mlbatbat://", safeLink: "https://apps.apple.com/us/app/mlb-at-bat/id493619333", type: .TicketedEvents, driverCategory: DriverCategory(id: .TicketedEvents, page: .travel, image: "")),
            Driver(id: "NHL", directLink: "NHL://", safeLink: "https://apps.apple.com/us/app/nhl/id465092669", type: .TicketedEvents, driverCategory: DriverCategory(id: .TicketedEvents, page: .travel, image: "")),
            Driver(id: "VEGAS", directLink: "fb785905631462078://", safeLink: "https://apps.apple.com/us/app/vegas-the-app/id886069412", type: .TicketedEvents, driverCategory: DriverCategory(id: .TicketedEvents, page: .travel, image: "")),
            Driver(id: "Las Vegas Event Maps", directLink: "", safeLink: "https://apps.apple.com/us/app/las-vegas-event-maps/id1464204033", type: .TicketedEvents, driverCategory: DriverCategory(id: .TicketedEvents, page: .travel, image: "")),
            Driver(id: "Oriental Institute Museum", directLink: "", safeLink: "https://apps.apple.com/us/app/oriental-institute-museum/id1192807803", type: .TicketedEvents, driverCategory: DriverCategory(id: .TicketedEvents, page: .travel, image: "")),
            Driver(id: "Station Casinos", directLink: "stnapp://", safeLink: "https://apps.apple.com/us/app/station-casinos-las-vegas/id730650062", type: .TicketedEvents, driverCategory: DriverCategory(id: .TicketedEvents, page: .travel, image: "")),
            Driver(id: "MGM Resorts", directLink: "mgm://", safeLink: "https://apps.apple.com/us/app/mgm-resorts-international/id366518979", type: .TicketedEvents, driverCategory: DriverCategory(id: .TicketedEvents, page: .travel, image: "")),
            Driver(id: "Las Vegas Map", directLink: "", safeLink: "https://apps.apple.com/us/app/las-vegas-map-and-walks/id349304516", type: .TicketedEvents, driverCategory: DriverCategory(id: .TicketedEvents, page: .travel, image: "")),
            
            
            
            
            
            
            

            
            //MARK: - Reviews
           // Driver(id: "Trip Advisor", directLink: "tripadvisor", safeLink: "https://apps.apple.com/us/app/tripadvisor-hotels-restaurants/id284876795", type: .Reviews, driverCategory: DriverCategory(id: .Reviews, page: .travel, image: "")),
            
            //MARK: - Coffee
          //  Driver(id: "Starbucks", directLink: "starbucks", safeLink: "https://apps.apple.com/us/app/starbucks/id331177714", type: .Coffee, driverCategory: DriverCategory(id: .Coffee, page: .travel, image: "")),
            //MARK: - Translator
           // Driver(id: "Speak & Translate", directLink: "translatorfreenew", safeLink: "", type: .Translator, driverCategory: DriverCategory(id: .Translator, page: .travel, image: "")),
        ])
    }
}
