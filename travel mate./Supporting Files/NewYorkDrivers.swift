//
//  DriverList.swift
//  Mate
//
//  Created by Landon Brambert on 9/16/19.
//  Copyright © 2019 Onyx Theaters.tv, INC. All rights reserved.
//

import Foundation
import HomeKit

class NewYorkDriverList {
    
    static var shared = NewYorkDriverList()
    
    let driverCategories = [
        DriverCategory(id: .Calendars, page: .weather, image: "camera"),
        DriverCategory(id: .Coffee, page: .weather, image: "camera"),
        DriverCategory(id: .Flights, page: .weather, image: "")
    ]
    
    let drivers: [Driver] = [
        //MARK: - Public Transportation
        Driver(id: "New York Subway MTA Map", directLink: "mxnewyorksubway://", safeLink: "https://apps.apple.com/us/app/new-york-subway-mta-map/id369691844", type: .PublicTransportation, driverCategory: DriverCategory(id: .PublicTransportation, page: .travel, image: "")),
        Driver(id: "NYC Subway Map", directLink: "newYorkTubeAppOpen://", safeLink: "https://apps.apple.com/us/app/new-york-city-subway-map/id683294660", type: .PublicTransportation, driverCategory: DriverCategory(id: .PublicTransportation, page: .travel, image: "")),
        Driver(id: "New York City Subway", directLink: "", safeLink: "https://apps.apple.com/us/app/new-york-city-subway/id599138866", type: .PublicTransportation, driverCategory: DriverCategory(id: .PublicTransportation, page: .travel, image: "")),
        Driver(id: "Transit • Bus & Subway Times", directLink: "transit://", safeLink: "https://apps.apple.com/us/app/transit-bus-subway-times/id498151501", type: .PublicTransportation, driverCategory: DriverCategory(id: .PublicTransportation, page: .travel, image: "")),
        
       
        
        //MARK: - RideShare
       // Driver(id: "Turo", directLink: "turo://", safeLink: "https://apps.apple.com/us/app/turo-better-than-car-rental/id555063314", type: .RideShare, driverCategory: DriverCategory(id: .RideShare, page: .travel, image: "")),
        
        //MARK: - Flights
       // Driver(id: "Expedia", directLink: "expda://", safeLink: "https://apps.apple.com/us/app/expedia-hotels-flights-car/id427916203", type: .Flights, driverCategory: DriverCategory(id: .Flights, page: .weather, image: "")),
        
        //MARK: - Hotels
       // Driver(id: "AirBnB", directLink: "airbnb", safeLink: "https://apps.apple.com/us/app/airbnb/id401626263", type: .Hotels, driverCategory: DriverCategory(id: .Hotels, page: .travel, image: "")),
        
       //MARK: - Calendars
      //  Driver(id: "", directLink: "", safeLink: "", type: .Calendars, driverCategory: DriverCategory(id: .Calendars, page: .travel, image: "")),
       //MARK: - Restaraunts
       // Driver(id: "", directLink: "", safeLink: "", type: .Restaraunts, driverCategory: DriverCategory(id: .Restaraunts, page: .travel, image: "")),
        //MARK: - Mobile Food
       // Driver(id: "Uber eats", directLink: "ubereats", safeLink: "https://apps.apple.com/us/app/uber-eats-food-delivery/id1058959277", type: .MobileFood, driverCategory: DriverCategory(id: .MobileFood, page: .travel, image: "")),
        
        //MARK: - TicketedEvents
        
        
        Driver(id: "New York Baseball - Yankees", directLink: "", safeLink: "https://apps.apple.com/us/app/new-york-baseball-yankees/id378084474", type: .TicketedEvents, driverCategory: DriverCategory(id: .TicketedEvents, page: .travel, image: "")),
        Driver(id: "New York Mets News", directLink: "net.advance.mets://", safeLink: "https://apps.apple.com/us/app/nj-com-new-york-mets-news/id513259342", type: .TicketedEvents, driverCategory: DriverCategory(id: .TicketedEvents, page: .travel, image: "")),
        Driver(id: "New York Knicks", directLink: "yc-nba-nyk://", safeLink: "https://apps.apple.com/us/app/new-york-knicks-official-app/id498010853", type: .TicketedEvents, driverCategory: DriverCategory(id: .TicketedEvents, page: .travel, image: "")),
        Driver(id: "Brooklyn Nets", directLink: "yc-nba-bkn://", safeLink: "https://apps.apple.com/us/app/brooklyn-nets/id570867939", type: .TicketedEvents, driverCategory: DriverCategory(id: .TicketedEvents, page: .travel, image: "")),
        Driver(id: "New York Giants", directLink: "yc-nfl-nyg://", safeLink: "https://apps.apple.com/us/app/new-york-giants/id462249608", type: .TicketedEvents, driverCategory: DriverCategory(id: .TicketedEvents, page: .travel, image: "")),
        Driver(id: "Official New York Jets", directLink: "yc-nfl-nyj://", safeLink: "https://apps.apple.com/us/app/official-new-york-jets/id460494117", type: .TicketedEvents, driverCategory: DriverCategory(id: .TicketedEvents, page: .travel, image: "")),
        Driver(id: "NHL", directLink: "NHL://", safeLink: "https://apps.apple.com/us/app/nhl/id465092669", type: .TicketedEvents, driverCategory: DriverCategory(id: .TicketedEvents, page: .travel, image: "")),
        Driver(id: "American Museum of Natural History", directLink: "amnh-explorer://", safeLink: "https://apps.apple.com/us/app/explorer-amnh-nyc/id381227123", type: .TicketedEvents, driverCategory: DriverCategory(id: .TicketedEvents, page: .travel, image: "")),
        Driver(id: "Metropolitan Museum of Art", directLink: "TRMexico://", safeLink: "https://apps.apple.com/us/app/metropolitan-museum-of-art-nyc/id977211908", type: .TicketedEvents, driverCategory: DriverCategory(id: .TicketedEvents, page: .travel, image: "")),
        Driver(id: "Zoo App", directLink: "", safeLink: "https://apps.apple.com/us/app/zoo-app-bronx-zoo-edition/id1473718369", type: .TicketedEvents, driverCategory: DriverCategory(id: .TicketedEvents, page: .travel, image: "")),
        Driver(id: "New York Pass", directLink: "nyp://", safeLink: "https://apps.apple.com/app/id429167326", type: .TicketedEvents, driverCategory: DriverCategory(id: .TicketedEvents, page: .travel, image: "")),
        Driver(id: "Central Park NYC ", directLink: "", safeLink: "https://apps.apple.com/us/app/central-park-nyc/id364718385", type: .TicketedEvents, driverCategory: DriverCategory(id: .TicketedEvents, page: .travel, image: "")),
        
        //MARK: - Reviews
      //  Driver(id: "Trip Advisor", directLink: "tripadvisor", safeLink: "https://apps.apple.com/us/app/tripadvisor-hotels-restaurants/id284876795", type: .Reviews, driverCategory: DriverCategory(id: .Reviews, page: .travel, image: "")),
        
        //MARK: - Coffee
      //  Driver(id: "Starbucks", directLink: "starbucks", safeLink: "https://apps.apple.com/us/app/starbucks/id331177714", type: .Coffee, driverCategory: DriverCategory(id: .Coffee, page: .travel, image: "")),
        //MARK: - Translator
      //  Driver(id: "Speak & Translate", directLink: "translatorfreenew", safeLink: "", type: .Translator, driverCategory: DriverCategory(id: .Translator, page: .travel, image: "")),
        
        
    ]
    
    func directDrivers() -> [Driver] {
        var directDrivers: [Driver] = []
        for driver in drivers {
            if driver.directLink != "" {
                directDrivers.append(driver)
            }
        }
        return directDrivers
    }
    
    func canOpen(_ driver: Driver) -> Bool {
        if let url = URL(string: driver.directLink) {
            return UIApplication.shared.canOpenURL(url)
        } else {
            return false
        }
    }
    
    func returnDrivers(by driverType: String) -> [Driver] {
        return drivers.filter({ $0.type.rawValue == driverType })
    }
    
    func returnDrivers(for page: Page) -> [Driver] {
        return drivers.filter({ $0.driverCategory?.page == page})
    }
    
    func driver(for key: String) -> Driver? {
        return drivers.first(where: { $0.id == key })
    }
    
    func returnDrivers(for category: DriverCategory) -> [Driver] {
        return drivers.filter({ $0.driverCategory?.image == category.image })
    }
    
}

struct Calibrator {
    private let drivers = DriverList()
    init() {
        for driver in drivers.directDrivers() {
            if self.drivers.canOpen(driver) {
                print("Setting default: \(driver.id) for category: \(driver.type.rawValue)")
//                defaults.set(driver.id, forKey: driver.type.rawValue)
            }
        }
    }
}
