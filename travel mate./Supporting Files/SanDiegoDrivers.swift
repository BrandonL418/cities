//
//  DriverList.swift
//  Mate
//
//  Created by Landon Brambert on 9/16/19.
//  Copyright © 2019 Onyx Theaters.tv, INC. All rights reserved.
//

import Foundation
import HomeKit

class SanDiegoDriverList {
    
    static var shared = SanDiegoDriverList()
    
    let driverCategories = [
        DriverCategory(id: .Calendars, page: .weather, image: "camera"),
        DriverCategory(id: .Coffee, page: .weather, image: "camera"),
        DriverCategory(id: .Flights, page: .weather, image: "")
    ]
    
    let drivers: [Driver] = [
        //MARK: - Public Transportation
        Driver(id: "My City Bikes San Diego", directLink: "", safeLink: "https://apps.apple.com/us/app/my-city-bikes-san-diego/id894459168", type: .PublicTransportation, driverCategory: DriverCategory(id: .PublicTransportation, page: .travel, image: "")),
        Driver(id: "Transit Tracker", directLink: "tt.san://", safeLink: "https://apps.apple.com/us/app/transit-tracker-san-diego/id939195911", type: .PublicTransportation, driverCategory: DriverCategory(id: .PublicTransportation, page: .travel, image: "")),
        
        
        
       
        
       
        
        //MARK: - RideShare
       // Driver(id: "Capital Bikeshare", directLink: "cabi://", safeLink: "https://apps.apple.com/us/app/capital-bikeshare/id1233403073", type: .RideShare, driverCategory: DriverCategory(id: .RideShare, page: .travel, image: "")),
        
        //MARK: - Flights
       // Driver(id: "Expedia", directLink: "expda://", safeLink: "https://apps.apple.com/us/app/expedia-hotels-flights-car/id427916203", type: .Flights, driverCategory: DriverCategory(id: .Flights, page: .weather, image: "")),
        
        //MARK: - Hotels
       // Driver(id: "AirBnB", directLink: "airbnb", safeLink: "https://apps.apple.com/us/app/airbnb/id401626263", type: .Hotels, driverCategory: DriverCategory(id: .Hotels, page: .travel, image: "")),
        
       //MARK: - Calendars
      //  Driver(id: "", directLink: "", safeLink: "", type: .Calendars, driverCategory: DriverCategory(id: .Calendars, page: .travel, image: "")),
       //MARK: - Restaraunts
      //  Driver(id: "", directLink: "", safeLink: "", type: .Restaraunts, driverCategory: DriverCategory(id: .Restaraunts, page: .travel, image: "")),
        //MARK: - Mobile Food
       // Driver(id: "Uber eats", directLink: "ubereats", safeLink: "https://apps.apple.com/us/app/uber-eats-food-delivery/id1058959277", type: .MobileFood, driverCategory: DriverCategory(id: .MobileFood, page: .travel, image: "")),
        
        //MARK: - TicketedEvents
        
        Driver(id: "MLB At Bat", directLink: "mlbatbat://", safeLink: "https://apps.apple.com/us/app/mlb-at-bat/id493619333", type: .TicketedEvents, driverCategory: DriverCategory(id: .TicketedEvents, page: .travel, image: "")),
        Driver(id: "NHL", directLink: "NHL://", safeLink: "https://apps.apple.com/us/app/nhl/id465092669", type: .TicketedEvents, driverCategory: DriverCategory(id: .TicketedEvents, page: .travel, image: "")),
        Driver(id: "San Diego Museum of Art", directLink: "fb458179701035428://", safeLink: "https://apps.apple.com/us/app/the-san-diego-museum-of-art/id1006902485", type: .TicketedEvents, driverCategory: DriverCategory(id: .TicketedEvents, page: .travel, image: "")),
        Driver(id: "San Diego Hikes", directLink: "com.roblabs.Black-Mountain://", safeLink: "https://apps.apple.com/us/app/san-diego-hikes/id1072564926", type: .TicketedEvents, driverCategory: DriverCategory(id: .TicketedEvents, page: .travel, image: "")),
        Driver(id: "Balboa Park", directLink: "fb775776472477196://", safeLink: "https://apps.apple.com/us/app/balboa-park-official-app/id970502543", type: .TicketedEvents, driverCategory: DriverCategory(id: .TicketedEvents, page: .travel, image: "")),
        Driver(id: "San Diego Zoo", directLink: "", safeLink: "https://apps.apple.com/us/app/san-diego-zoo-travel-guide/id1438051921", type: .TicketedEvents, driverCategory: DriverCategory(id: .TicketedEvents, page: .travel, image: "")),
        
        
        
        
        
        //MARK: - Reviews
       // Driver(id: "Trip Advisor", directLink: "tripadvisor", safeLink: "https://apps.apple.com/us/app/tripadvisor-hotels-restaurants/id284876795", type: .Reviews, driverCategory: DriverCategory(id: .Reviews, page: .travel, image: "")),
        
        //MARK: - Coffee
      //  Driver(id: "Starbucks", directLink: "starbucks", safeLink: "https://apps.apple.com/us/app/starbucks/id331177714", type: .Coffee, driverCategory: DriverCategory(id: .Coffee, page: .travel, image: "")),
        //MARK: - Translator
       // Driver(id: "Speak & Translate", directLink: "translatorfreenew", safeLink: "", type: .Translator, driverCategory: DriverCategory(id: .Translator, page: .travel, image: "")),
        
        
    ]
    
    func directDrivers() -> [Driver] {
        var directDrivers: [Driver] = []
        for driver in drivers {
            if driver.directLink != "" {
                directDrivers.append(driver)
            }
        }
        return directDrivers
    }
    
    func canOpen(_ driver: Driver) -> Bool {
        if let url = URL(string: driver.directLink) {
            return UIApplication.shared.canOpenURL(url)
        } else {
            return false
        }
    }
    
    func returnDrivers(by driverType: String) -> [Driver] {
        return drivers.filter({ $0.type.rawValue == driverType })
    }
    
    func returnDrivers(for page: Page) -> [Driver] {
        return drivers.filter({ $0.driverCategory?.page == page})
    }
    
    func driver(for key: String) -> Driver? {
        return drivers.first(where: { $0.id == key })
    }
    
    func returnDrivers(for category: DriverCategory) -> [Driver] {
        return drivers.filter({ $0.driverCategory?.image == category.image })
    }
    
}

struct Calibrator {
    private let drivers = DriverList()
    init() {
        for driver in drivers.directDrivers() {
            if self.drivers.canOpen(driver) {
                print("Setting default: \(driver.id) for category: \(driver.type.rawValue)")
//                defaults.set(driver.id, forKey: driver.type.rawValue)
            }
        }
    }
}
