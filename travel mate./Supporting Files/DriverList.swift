//
//  DriverList.swift
//  Mate
//
//  Created by Landon Brambert on 9/16/19.
//  Copyright © 2019 Onyx Theaters.tv, INC. All rights reserved.
//

import Foundation
import HomeKit

class DriverList {
    
    static var shared = DriverList()
    
    let driverCategories = [
        DriverCategory(id: .Calendars, page: .weather, image: "camera"),
        DriverCategory(id: .Coffee, page: .weather, image: "camera"),
        DriverCategory(id: .Flights, page: .weather, image: "")
    ]
    
    let drivers: [Driver] = [
        //MARK: - Public Transportation
        
        Driver(id: "Parking Panda", directLink: "parkingpanda://", safeLink: "https://apps.apple.com/us/app/parking-panda-on-demand-parking-deals/id550285323", type: .PublicTransportation, driverCategory: DriverCategory(id: .PublicTransportation, page: .travel, image: "")),
       
        
        //MARK: - RideShare
        Driver(id: "Turo", directLink: "turo://", safeLink: "https://apps.apple.com/us/app/turo-better-than-car-rental/id555063314", type: .RideShare, driverCategory: DriverCategory(id: .RideShare, page: .travel, image: "")),
        Driver(id: "Uber", directLink: "uber://", safeLink: "https://apps.apple.com/us/app/uber/id368677368", type: .RideShare, driverCategory: DriverCategory(id: .RideShare, page: .travel, image: "")),
        Driver(id: "Lyft", directLink: "lyft://", safeLink: "https://apps.apple.com/us/app/lyft/id529379082", type: .RideShare, driverCategory: DriverCategory(id: .RideShare, page: .travel, image: "")),
        Driver(id: "Flywheel", directLink: "flywheelSports://", safeLink: "https://apps.apple.com/us/app/flywheel-the-taxi-app/id584165682", type: .RideShare, driverCategory: DriverCategory(id: .RideShare, page: .travel, image: "")),
        
        Driver(id: "Bird", directLink: "bird://", safeLink: "https://apps.apple.com/tr/app/bird-enjoy-the-ride/id1260842311", type: .RideShare, driverCategory: DriverCategory(id: .RideShare, page: .travel, image: "")),
        Driver(id: "Lime", directLink: "limebike://", safeLink: "https://apps.apple.com/us/app/lime-your-ride-anytime/id1199780189", type: .RideShare, driverCategory: DriverCategory(id: .RideShare, page: .travel, image: "")),
        Driver(id: "National Car Rental ", directLink: "nationalmobileapp://", safeLink: "https://apps.apple.com/us/app/national-car-rental/id675304115", type: .RideShare, driverCategory: DriverCategory(id: .RideShare, page: .travel, image: "")),
        //MARK: - Flights
        Driver(id: "Expedia", directLink: "expda://", safeLink: "https://apps.apple.com/us/app/expedia-hotels-flights-car/id427916203", type: .Flights, driverCategory: DriverCategory(id: .Flights, page: .weather, image: "")),
        Driver(id: "Kayak", directLink: "kayak://", safeLink: "https://apps.apple.com/us/app/kayak-flights-hotels-cars/id305204535", type: .Flights, driverCategory: DriverCategory(id: .Flights, page: .weather, image: "")),
        Driver(id: "Southwest", directLink: "southwest://", safeLink: "https://apps.apple.com/us/app/southwest-airlines/id344542975", type: .Flights, driverCategory: DriverCategory(id: .Flights, page: .weather, image: "")),
        Driver(id: "United", directLink: "ualmobile://", safeLink: "https://apps.apple.com/us/app/united-airlines/id449945214", type: .Flights, driverCategory: DriverCategory(id: .Flights, page: .weather, image: "")),
        Driver(id: "Travelocity", directLink: "tvly://", safeLink: "https://apps.apple.com/us/app/travelocity-hotels-flights/id284803487", type: .Flights, driverCategory: DriverCategory(id: .Flights, page: .weather, image: "")),
        Driver(id: "Virgin", directLink: "virginmobile://", safeLink: "https://apps.apple.com/us/app/virgin-mobile-my-account/id853116586", type: .Flights, driverCategory: DriverCategory(id: .Flights, page: .weather, image: "")),
        Driver(id: "Fly Delta", directLink: "deltastudio://", safeLink: "https://apps.apple.com/us/app/fly-delta/id388491656", type: .Flights, driverCategory: DriverCategory(id: .Flights, page: .weather, image: "")),
        Driver(id: "JetBlue", directLink: "jetblue://", safeLink: "https://apps.apple.com/us/app/jetblue-book-manage-trips/id481370590", type: .Flights, driverCategory: DriverCategory(id: .Flights, page: .weather, image: "")),
        Driver(id: "Frontier Airlines", directLink: "", safeLink: "https://apps.apple.com/us/app/frontier-airlines/id1041410548", type: .Flights, driverCategory: DriverCategory(id: .Flights, page: .weather, image: "")),
        Driver(id: "Mobile Passport", directLink: "mobilepassportus://", safeLink: "https://apps.apple.com/us/app/mobile-passport/id907024887", type: .Flights, driverCategory: DriverCategory(id: .Flights, page: .weather, image: "")),
        Driver(id: "Alaska Airlines", directLink: "", safeLink: "https://apps.apple.com/us/app/alaska-airlines/id356143077", type: .Flights, driverCategory: DriverCategory(id: .Flights, page: .weather, image: "")),
        Driver(id: "Spirit Airlines", directLink: "spiritapp://", safeLink: "https://apps.apple.com/us/app/spirit-airlines/id1438670520", type: .Flights, driverCategory: DriverCategory(id: .Flights, page: .weather, image: "")),
        Driver(id: "Flightaware", directLink: "flightaware://", safeLink: "https://apps.apple.com/us/app/flightaware-flight-tracker/id316793974", type: .Flights, driverCategory: DriverCategory(id: .Flights, page: .travel, image: "")),
        
        //MARK: - Hotels
        Driver(id: "AirBnB", directLink: "airbnb://", safeLink: "https://apps.apple.com/us/app/airbnb/id401626263", type: .Hotels, driverCategory: DriverCategory(id: .Hotels, page: .travel, image: "")),
        Driver(id: "Hilton", directLink: "hhonors://", safeLink: "https://apps.apple.com/us/app/hilton-honors-book-hotels/id635150066", type: .Hotels, driverCategory: DriverCategory(id: .Hotels, page: .travel, image: "")),
        Driver(id: "Marriott", directLink: "marriott://", safeLink: "https://apps.apple.com/us/app/marriott-bonvoy/id455004730", type: .Hotels, driverCategory: DriverCategory(id: .Hotels, page: .travel, image: "")),
        Driver(id: "Hotel Tonight", directLink: "hoteltonight://", safeLink: "https://apps.apple.com/us/app/hoteltonight-hotel-deals/id407690035", type: .Hotels, driverCategory: DriverCategory(id: .Hotels, page: .travel, image: "")),
        Driver(id: "Hotels.com", directLink: "hotelsapp://", safeLink: "https://apps.apple.com/us/app/hotels-com-book-hotels-more/id284971959", type: .Hotels, driverCategory: DriverCategory(id: .Hotels, page: .travel, image: "")),
        Driver(id: "trivago", directLink: "trivago://", safeLink: "https://apps.apple.com/us/app/trivago-compare-hotel-prices/id376888389", type: .Hotels, driverCategory: DriverCategory(id: .Hotels, page: .travel, image: "")),
        Driver(id: "Choice Hotels", directLink: "choicehotels://", safeLink: "https://apps.apple.com/us/app/choice-hotels/id509342785", type: .Hotels, driverCategory: DriverCategory(id: .Hotels, page: .travel, image: "")),
        Driver(id: "IHG", directLink: "ihgfind://", safeLink: "https://apps.apple.com/us/app/ihg-hotel-deals-rewards/id368217298", type: .Hotels, driverCategory: DriverCategory(id: .Hotels, page: .travel, image: "")),
        Driver(id: "Wyndham Rewards", directLink: "", safeLink: "https://apps.apple.com/us/app/wyndham-rewards/id783649785", type: .Hotels, driverCategory: DriverCategory(id: .Hotels, page: .travel, image: "")),
        Driver(id: "Orbitz", directLink: "obtz://", safeLink: "https://apps.apple.com/us/app/orbitz-hotels-flights/id403546234", type: .Hotels, driverCategory: DriverCategory(id: .Hotels, page: .travel, image: "")),
        
       //MARK: - Calendars
        Driver(id: "Google Calendar", directLink: "googlecalendar://", safeLink: "https://apps.apple.com/us/app/google-calendar-time-planner/id909319292", type: .Calendars, driverCategory: DriverCategory(id: .Calendars, page: .travel, image: "")),
        Driver(id: "TripIt", directLink: "tripit://", safeLink: "https://apps.apple.com/us/app/tripit-travel-planner/id311035142", type: .Calendars, driverCategory: DriverCategory(id: .Calendars, page: .weather, image: "")),
        Driver(id: "My Travel Planner", directLink: "fb109562305859743://", safeLink: "https://apps.apple.com/us/app/my-travel-planner-app/id1450534915", type: .Calendars, driverCategory: DriverCategory(id: .Calendars, page: .weather, image: "")),
        Driver(id: "Roadtrippers", directLink: "roadtrippers://", safeLink: "https://apps.apple.com/us/app/roadtrippers-trip-planner/id944060491", type: .Calendars, driverCategory: DriverCategory(id: .Calendars, page: .weather, image: "")),
        Driver(id: "Welcome", directLink: "welcome://", safeLink: "https://apps.apple.com/us/app/welcome-ai-itineraries/id1447252527", type: .Calendars, driverCategory: DriverCategory(id: .Calendars, page: .weather, image: "")),
        Driver(id: "Travel Planner", directLink: "", safeLink: "https://apps.apple.com/us/app/travel-planner/id1454922849", type: .Calendars, driverCategory: DriverCategory(id: .Calendars, page: .weather, image: "")),
        Driver(id: "Planner Pro", directLink: "welcome://", safeLink: "https://apps.apple.com/us/app/planner-pro-daily-calendar/id571588936", type: .Calendars, driverCategory: DriverCategory(id: .Calendars, page: .weather, image: "")),
        Driver(id: "Calendar", directLink: "calshow://", safeLink: "https://apps.apple.com/us/app/calendar/id1108185179", type: .Calendars, driverCategory: DriverCategory(id: .Calendars, page: .weather, image: "")),
        Driver(id: "AAA Mobile", directLink: "aaamobile://", safeLink: "https://apps.apple.com/us/app/aaa-mobile/id310730297", type: .Calendars, driverCategory: DriverCategory(id: .Calendars, page: .weather, image: "")),
       //MARK: - Restaraunts
        Driver(id: "OpenTable", directLink: "opentable://", safeLink: "https://apps.apple.com/us/app/opentable/id296581815", type: .Restaraunts, driverCategory: DriverCategory(id: .Restaraunts, page: .travel, image: "")),
        Driver(id: "Resy", directLink: "resy://", safeLink: "https://apps.apple.com/us/app/resy/id866163372", type: .Restaraunts, driverCategory: DriverCategory(id: .Restaraunts, page: .travel, image: "")),
        Driver(id: "Yelp Waitlist", directLink: "", safeLink: "https://apps.apple.com/us/app/yelp-waitlist-nowait/id404226510", type: .Restaraunts, driverCategory: DriverCategory(id: .Restaraunts, page: .travel, image: "")),
        Driver(id: "Seated", directLink: "voco://", safeLink: "https://apps.apple.com/us/app/seated-cash-back-for-foodies/id1031760854", type: .Restaraunts, driverCategory: DriverCategory(id: .Restaraunts, page: .travel, image: "")),
        Driver(id: "ReserveOut ", directLink: "", safeLink: "https://apps.apple.com/us/app/reserveout/id736004914", type: .Restaraunts, driverCategory: DriverCategory(id: .Restaraunts, page: .travel, image: "")),
        Driver(id: "Tock", directLink: "fb319973538191556://", safeLink: "https://apps.apple.com/us/app/tock-restaurant-reservations/id1369347408", type: .Restaraunts, driverCategory: DriverCategory(id: .Restaraunts, page: .travel, image: "")),
        Driver(id: "Foursquare", directLink: "", safeLink: "https://apps.apple.com/us/app/foursquare-city-guide/id306934924", type: .Restaraunts, driverCategory: DriverCategory(id: .Restaraunts, page: .travel, image: "")),
        //MARK: - Mobile Food
        Driver(id: "Uber eats", directLink: "ubereats://", safeLink: "https://apps.apple.com/us/app/uber-eats-food-delivery/id1058959277", type: .MobileFood, driverCategory: DriverCategory(id: .MobileFood, page: .travel, image: "")),
        Driver(id: "Postmates", directLink: "", safeLink: "https://apps.apple.com/us/app/postmates-food-delivery/id512393983", type: .MobileFood, driverCategory: DriverCategory(id: .MobileFood, page: .travel, image: "")),
        Driver(id: "Door Dash", directLink: "doordash://", safeLink: "https://apps.apple.com/us/app/doordash-food-delivery/id719972451", type: .MobileFood, driverCategory: DriverCategory(id: .MobileFood, page: .travel, image: "")),
        Driver(id: "Instacart", directLink: "instacart://", safeLink: "https://apps.apple.com/us/app/instacart/id545599256", type: .MobileFood, driverCategory: DriverCategory(id: .MobileFood, page: .travel, image: "")),
        Driver(id: "Amazon Prime Now (Food Delivery)", directLink: "primenow://", safeLink: "https://apps.apple.com/us/app/amazon-prime-now/id947644950", type: .MobileFood, driverCategory: DriverCategory(id: .MobileFood, page: .travel, image: "")),
        Driver(id: "Zomato", directLink: "zomato://", safeLink: "https://apps.apple.com/in/app/zomato-food-restaurant-finder/id434613896", type: .MobileFood, driverCategory: DriverCategory(id: .MobileFood, page: .travel, image: "")),
        //MARK: - TicketedEvents
        Driver(id: "Viator", directLink: "viator://", safeLink: "https://apps.apple.com/us/app/viator-tours-activities/id434832826", type: .TicketedEvents, driverCategory: DriverCategory(id: .TicketedEvents, page: .travel, image: "")),
        Driver(id: "Stubhub", directLink: "stubhub://", safeLink: "https://apps.apple.com/us/app/stubhub-event-tickets/id366562751", type: .TicketedEvents, driverCategory: DriverCategory(id: .TicketedEvents, page: .travel, image: "")),
        Driver(id: "Bandsintown", directLink: "bitsnap://", safeLink: "https://apps.apple.com/us/app/bandsintown-concerts/id471394851", type: .TicketedEvents, driverCategory: DriverCategory(id: .TicketedEvents, page: .travel, image: "")),
        Driver(id: "G. Spotting", directLink: "", safeLink: "https://apps.apple.com/us/app/g-spotting/id484566289", type: .TicketedEvents, driverCategory: DriverCategory(id: .TicketedEvents, page: .travel, image: "")),
        //MARK: - Reviews
        Driver(id: "Trip Advisor", directLink: "tripadvisor://", safeLink: "https://apps.apple.com/us/app/tripadvisor-hotels-restaurants/id284876795", type: .Reviews, driverCategory: DriverCategory(id: .Reviews, page: .travel, image: "")),
        Driver(id: "Zagat", directLink: "zagat://", safeLink: "https://apps.apple.com/us/app/zagat/id669821485", type: .Reviews, driverCategory: DriverCategory(id: .Reviews, page: .travel, image: "")),
        Driver(id: "Yelp", directLink: "yelp://", safeLink: "https://apps.apple.com/us/app/yelp-food-services-around-me/id284910350", type: .Reviews, driverCategory: DriverCategory(id: .Reviews, page: .travel, image: "")),
        
        //MARK: - Coffee
        Driver(id: "Starbucks", directLink: "starbucks://", safeLink: "https://apps.apple.com/us/app/starbucks/id331177714", type: .Coffee, driverCategory: DriverCategory(id: .Coffee, page: .travel, image: "")),
        Driver(id: "Caribou Coffee", directLink: "caribouapp://", safeLink: "https://apps.apple.com/us/app/caribou-coffee/id971358255", type: .Coffee, driverCategory: DriverCategory(id: .Coffee, page: .travel, image: "")),
        Driver(id: "CUPS", directLink: "cups://", safeLink: "https://apps.apple.com/us/app/cups-your-local-coffee-fix/id556462755", type: .Coffee, driverCategory: DriverCategory(id: .Coffee, page: .travel, image: "")),
        Driver(id: "Dunkin'", directLink: "dunkindonuts://", safeLink: "https://apps.apple.com/us/app/dunkin/id1056813463", type: .Coffee, driverCategory: DriverCategory(id: .Coffee, page: .travel, image: "")),
        Driver(id: "Peet’s Coffee", directLink: "nomnom://", safeLink: "https://apps.apple.com/us/app/peets-coffee/id1059865964", type: .Coffee, driverCategory: DriverCategory(id: .Coffee, page: .travel, image: "")),
        Driver(id: "Einstein Bros Bagels", directLink: "cashstarCARIBOUCOFFEE://", safeLink: "https://apps.apple.com/us/app/einstein-bros-bagels/id1405128897", type: .Coffee, driverCategory: DriverCategory(id: .Coffee, page: .travel, image: "")),
        //MARK: - Translator
        Driver(id: "Speak & Translate", directLink: "translatorfreenew://", safeLink: "", type: .Translator, driverCategory: DriverCategory(id: .Translator, page: .travel, image: "")),
        
        
    ]
    
    func directDrivers() -> [Driver] {
        var directDrivers: [Driver] = []
        for driver in drivers {
            if driver.directLink != "" {
                directDrivers.append(driver)
            }
        }
        return directDrivers
    }
    
    func canOpen(_ driver: Driver) -> Bool {
        if let url = URL(string: driver.directLink) {
            return UIApplication.shared.canOpenURL(url)
        } else {
            return false
        }
    }
    
    func returnDrivers(by driverType: String) -> [Driver] {
        return drivers.filter({ $0.type.rawValue == driverType })
    }
    
    func returnDrivers(for page: Page) -> [Driver] {
        return drivers.filter({ $0.driverCategory?.page == page})
    }
    
    func driver(for key: String) -> Driver? {
        return drivers.first(where: { $0.id == key })
    }
    
    func returnDrivers(for category: DriverCategory) -> [Driver] {
        return drivers.filter({ $0.driverCategory?.image == category.image })
    }
    
}

struct Calibrator {
    private let drivers = DriverList()
    init() {
        for driver in drivers.directDrivers() {
            if self.drivers.canOpen(driver) {
                print("Setting default: \(driver.id) for category: \(driver.type.rawValue)")
//                defaults.set(driver.id, forKey: driver.type.rawValue)
            }
        }
    }
}
