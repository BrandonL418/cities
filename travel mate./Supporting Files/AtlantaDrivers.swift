//
//  DriverList.swift
//  Mate
//
//  Created by Landon Brambert on 9/16/19.
//  Copyright © 2019 Onyx Theaters.tv, INC. All rights reserved.
//

import Foundation
import HomeKit

class Atlanta: Drivers {
    init() {
        super.init(drivers: [
            //MARK: - Public Transportation
            Driver(id: "Atlanta Transit", directLink: "", safeLink: "https://apps.apple.com/us/app/atlanta-transit-marta-times/id1176568695", type: .PublicTransportation, driverCategory: DriverCategory(id: .PublicTransportation, page: .travel, image: "")),
            Driver(id: "MARTA", directLink: "martaApp://", safeLink: "https://apps.apple.com/us/app/marta-on-the-go/id386648039", type: .PublicTransportation, driverCategory: DriverCategory(id: .PublicTransportation, page: .travel, image: "")),
            
            
            
            
            //MARK: - RideShare
            Driver(id: "Social Bicycles", directLink: "sobi://", safeLink: "https://apps.apple.com/us/app/social-bicycles/id641497286", type: .RideShare, driverCategory: DriverCategory(id: .RideShare, page: .travel, image: "")),
            
            //MARK: - Flights
          //  Driver(id: "CLT Airport", directLink: "", safeLink: "https://apps.apple.com/us/app/clt-airport/id1459226423", type: .Flights, driverCategory: DriverCategory(id: .Flights, page: .weather, image: "")),
            
            //MARK: - Hotels
           // Driver(id: "AirBnB", directLink: "airbnb", safeLink: "https://apps.apple.com/us/app/airbnb/id401626263", type: .Hotels, driverCategory: DriverCategory(id: .Hotels, page: .travel, image: "")),
            
           //MARK: - Calendars
           // Driver(id: "Visit Arizona ", directLink: "", safeLink: "https://apps.apple.com/us/app/visit-arizona/id1315756547", type: .Calendars, driverCategory: DriverCategory(id: .Calendars, page: .travel, image: "")),
           //MARK: - Restaraunts
            Driver(id: "Hand-Picked", directLink: "scoutmobhp://", safeLink: "https://apps.apple.com/us/app/hand-picked-atlanta/id346700012", type: .Restaraunts, driverCategory: DriverCategory(id: .Restaraunts, page: .travel, image: "")),
            //MARK: - Mobile Food
           // Driver(id: "Uber eats", directLink: "ubereats", safeLink: "https://apps.apple.com/us/app/uber-eats-food-delivery/id1058959277", type: .MobileFood, driverCategory: DriverCategory(id: .MobileFood, page: .travel, image: "")),
            
            //MARK: - TicketedEvents
            
            Driver(id: "MLB At Bat", directLink: "mlbatbat://", safeLink: "https://apps.apple.com/us/app/mlb-at-bat/id493619333", type: .TicketedEvents, driverCategory: DriverCategory(id: .TicketedEvents, page: .travel, image: "")),
            Driver(id: "NHL", directLink: "NHL://", safeLink: "https://apps.apple.com/us/app/nhl/id465092669", type: .TicketedEvents, driverCategory: DriverCategory(id: .TicketedEvents, page: .travel, image: "")),
            Driver(id: "Hawks", directLink: "hawksapp://", safeLink: "https://apps.apple.com/us/app/hawks-state-farm-arena/id979576290", type: .TicketedEvents, driverCategory: DriverCategory(id: .TicketedEvents, page: .travel, image: "")),
            Driver(id: "Atlanta Falcons", directLink: "falconsfr://", safeLink: "https://apps.apple.com/us/app/atlanta-falcons/id1170676367", type: .TicketedEvents, driverCategory: DriverCategory(id: .TicketedEvents, page: .travel, image: "")),
            

            
            //MARK: - Reviews
           // Driver(id: "Trip Advisor", directLink: "tripadvisor", safeLink: "https://apps.apple.com/us/app/tripadvisor-hotels-restaurants/id284876795", type: .Reviews, driverCategory: DriverCategory(id: .Reviews, page: .travel, image: "")),
            
            //MARK: - Coffee
          //  Driver(id: "Starbucks", directLink: "starbucks", safeLink: "https://apps.apple.com/us/app/starbucks/id331177714", type: .Coffee, driverCategory: DriverCategory(id: .Coffee, page: .travel, image: "")),
            //MARK: - Translator
           // Driver(id: "Speak & Translate", directLink: "translatorfreenew", safeLink: "", type: .Translator, driverCategory: DriverCategory(id: .Translator, page: .travel, image: "")),
        ])
    }
}
