//
//  ContentView.swift
//  travel mate.
//
//  Created by Landon Brambert on 10/7/19.
//  Copyright © 2019 ONYX. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello World")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
