//
//  DriverList.swift
//  Mate
//
//  Created by Landon Brambert on 9/16/19.
//  Copyright © 2019 Onyx Theaters.tv, INC. All rights reserved.
//

import Foundation
import HomeKit

class OklahomaCity: Drivers {
    init() {
        super.init(drivers: [
            //MARK: - Public Transportation
            Driver(id: "EMBARK Connect", directLink: "fb359210164172988://", safeLink: "https://apps.apple.com/us/app/embark-connect/id849991213", type: .PublicTransportation, driverCategory: DriverCategory(id: .PublicTransportation, page: .travel, image: "")),
            Driver(id: "TravelOK", directLink: "fb785611191452738travelokapp://", safeLink: "https://apps.apple.com/us/app/travelok/id1281865917", type: .PublicTransportation, driverCategory: DriverCategory(id: .PublicTransportation, page: .travel, image: "")),
            
            //MARK: - RideShare
         //   Driver(id: "BikeTown", directLink: "biketownpdx://", safeLink: "https://apps.apple.com/us/app/capital-bikeshare/id1233403073", type: .RideShare, driverCategory: DriverCategory(id: .RideShare, page: .travel, image: "")),
            
            //MARK: - Flights
           // Driver(id: "Expedia", directLink: "expda://", safeLink: "https://apps.apple.com/us/app/expedia-hotels-flights-car/id427916203", type: .Flights, driverCategory: DriverCategory(id: .Flights, page: .weather, image: "")),
            
            //MARK: - Hotels
           // Driver(id: "AirBnB", directLink: "airbnb", safeLink: "https://apps.apple.com/us/app/airbnb/id401626263", type: .Hotels, driverCategory: DriverCategory(id: .Hotels, page: .travel, image: "")),
            
           //MARK: - Calendars
          //  Driver(id: "", directLink: "", safeLink: "", type: .Calendars, driverCategory: DriverCategory(id: .Calendars, page: .travel, image: "")),
           //MARK: - Restaraunts
          //  Driver(id: "", directLink: "", safeLink: "", type: .Restaraunts, driverCategory: DriverCategory(id: .Restaraunts, page: .travel, image: "")),
            //MARK: - Mobile Food
           // Driver(id: "Uber eats", directLink: "ubereats", safeLink: "https://apps.apple.com/us/app/uber-eats-food-delivery/id1058959277", type: .MobileFood, driverCategory: DriverCategory(id: .MobileFood, page: .travel, image: "")),
            
            //MARK: - TicketedEvents
            
            
            Driver(id: "SoonerApp", directLink: "soonerapp://", safeLink: "https://apps.apple.com/us/app/soonerapp-oklahoma-football/id550328984", type: .TicketedEvents, driverCategory: DriverCategory(id: .TicketedEvents, page: .travel, image: "")),
            Driver(id: "Oklahoma City Thunder", directLink: "yc-nba-okc://", safeLink: "https://apps.apple.com/us/app/oklahoma-city-thunder/id731118829", type: .TicketedEvents, driverCategory: DriverCategory(id: .TicketedEvents, page: .travel, image: "")),
            Driver(id: "OKCMOA", directLink: "fb723533051015974museum97://", safeLink: "https://apps.apple.com/us/app/okcmoa/id1295786502", type: .TicketedEvents, driverCategory: DriverCategory(id: .TicketedEvents, page: .travel, image: "")),
            Driver(id: "OKC Zoo", directLink: "", safeLink: "https://apps.apple.com/us/app/okc-zoo/id1087376499", type: .TicketedEvents, driverCategory: DriverCategory(id: .TicketedEvents, page: .travel, image: "")),
            //MARK: - Reviews
           // Driver(id: "Trip Advisor", directLink: "tripadvisor", safeLink: "https://apps.apple.com/us/app/tripadvisor-hotels-restaurants/id284876795", type: .Reviews, driverCategory: DriverCategory(id: .Reviews, page: .travel, image: "")),
            
            //MARK: - Coffee
          //  Driver(id: "Starbucks", directLink: "starbucks", safeLink: "https://apps.apple.com/us/app/starbucks/id331177714", type: .Coffee, driverCategory: DriverCategory(id: .Coffee, page: .travel, image: "")),
            //MARK: - Translator
           // Driver(id: "Speak & Translate", directLink: "translatorfreenew", safeLink: "", type: .Translator, driverCategory: DriverCategory(id: .Translator, page: .travel, image: "")),
        ])
    }
}
