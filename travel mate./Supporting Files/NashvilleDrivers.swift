//
//  DriverList.swift
//  Mate
//
//  Created by Landon Brambert on 9/16/19.
//  Copyright © 2019 Onyx Theaters.tv, INC. All rights reserved.
//

import Foundation
import HomeKit

class Nashville: Drivers {
    init() {
        super.init(drivers: [
            //MARK: - Public Transportation
            Driver(id: "Transit Tracker", directLink: "tt.sat://", safeLink: "https://apps.apple.com/us/app/transit-tracker-san-antonio/id995691966", type: .PublicTransportation, driverCategory: DriverCategory(id: .PublicTransportation, page: .travel, image: "")),
            
            
            
            
            
            
            
            //MARK: - RideShare
           
            Driver(id: "BCycle", directLink: "bcycle://", safeLink: "https://apps.apple.com/us/app/b-cycle/id371185597", type: .RideShare, driverCategory: DriverCategory(id: .RideShare, page: .travel, image: "")),
            
            //MARK: - Flights
        //    Driver(id: "Orlando MCO", directLink: "", safeLink: "https://apps.apple.com/us/app/orlando-mco/id939593688", type: .Flights, driverCategory: DriverCategory(id: .Flights, page: .weather, image: "")),
            
            //MARK: - Hotels
         //   Driver(id: "Orlando World Center Marriott", directLink: "mwcwayfinding", safeLink: "https://apps.apple.com/us/app/orlando-world-center-marriott/id1311856070", type: .Hotels, driverCategory: DriverCategory(id: .Hotels, page: .travel, image: "")),
            
           //MARK: - Calendars
           // Driver(id: "Visit Orlando", directLink: "visitorlando://", safeLink: "https://apps.apple.com/us/app/visit-orlando-app/id1122736071", type: .Calendars, driverCategory: DriverCategory(id: .Calendars, page: .travel, image: "")),
            
           //MARK: - Restaraunts
           // Driver(id: "Hand-Picked", directLink: "scoutmobhp://", safeLink: "https://apps.apple.com/us/app/hand-picked-atlanta/id346700012", type: .Restaraunts, driverCategory: DriverCategory(id: .Restaraunts, page: .travel, image: "")),
            //MARK: - Mobile Food
           // Driver(id: "Uber eats", directLink: "ubereats", safeLink: "https://apps.apple.com/us/app/uber-eats-food-delivery/id1058959277", type: .MobileFood, driverCategory: DriverCategory(id: .MobileFood, page: .travel, image: "")),
            
            //MARK: - TicketedEvents
            
            Driver(id: "MLB At Bat", directLink: "mlbatbat://", safeLink: "https://apps.apple.com/us/app/mlb-at-bat/id493619333", type: .TicketedEvents, driverCategory: DriverCategory(id: .TicketedEvents, page: .travel, image: "")),
            Driver(id: "NHL", directLink: "NHL://", safeLink: "https://apps.apple.com/us/app/nhl/id465092669", type: .TicketedEvents, driverCategory: DriverCategory(id: .TicketedEvents, page: .travel, image: "")),
            Driver(id: "Tennessee Titans", directLink: "yc-nfl-ten://", safeLink: "https://apps.apple.com/us/app/tennessee-titans-mobile/id557699031", type: .TicketedEvents, driverCategory: DriverCategory(id: .TicketedEvents, page: .travel, image: "")),
            Driver(id: "The Frist", directLink: "fb723533051015974museum64://", safeLink: "https://apps.apple.com/us/app/the-frist/id1147718858", type: .TicketedEvents, driverCategory: DriverCategory(id: .TicketedEvents, page: .travel, image: "")),
            Driver(id: "YMCA", directLink: "com.netpulse.ymcaofmiddletennessee://", safeLink: "https://apps.apple.com/us/app/ymca-of-middle-tennessee/id1457981452", type: .TicketedEvents, driverCategory: DriverCategory(id: .TicketedEvents, page: .travel, image: "")),
            
            
            
            
            

            
            //MARK: - Reviews
           // Driver(id: "Trip Advisor", directLink: "tripadvisor", safeLink: "https://apps.apple.com/us/app/tripadvisor-hotels-restaurants/id284876795", type: .Reviews, driverCategory: DriverCategory(id: .Reviews, page: .travel, image: "")),
            
            //MARK: - Coffee
          //  Driver(id: "Starbucks", directLink: "starbucks", safeLink: "https://apps.apple.com/us/app/starbucks/id331177714", type: .Coffee, driverCategory: DriverCategory(id: .Coffee, page: .travel, image: "")),
            //MARK: - Translator
           // Driver(id: "Speak & Translate", directLink: "translatorfreenew", safeLink: "", type: .Translator, driverCategory: DriverCategory(id: .Translator, page: .travel, image: "")),
        ])
    }
}
