//
//  DriverList.swift
//  Mate
//
//  Created by Landon Brambert on 9/16/19.
//  Copyright © 2019 Onyx Theaters.tv, INC. All rights reserved.
//

import Foundation
import HomeKit

class Orlando: Drivers {
    init() {
        super.init(drivers: [
            //MARK: - Public Transportation
            Driver(id: "HOPR Transit", directLink: "hopr://", safeLink: "https://apps.apple.com/app/apple-store/id1330690892", type: .PublicTransportation, driverCategory: DriverCategory(id: .PublicTransportation, page: .travel, image: "")),
            Driver(id: "SunRail", directLink: "", safeLink: "https://apps.apple.com/us/app/sunrail/id1457771380", type: .PublicTransportation, driverCategory: DriverCategory(id: .PublicTransportation, page: .travel, image: "")),
            
            
            
            
            
            
            //MARK: - RideShare
          //  Driver(id: "BCycle", directLink: "bcycle://", safeLink: "https://apps.apple.com/us/app/b-cycle/id371185597", type: .RideShare, driverCategory: DriverCategory(id: .RideShare, page: .travel, image: "")),
            
            //MARK: - Flights
            Driver(id: "Orlando MCO", directLink: "", safeLink: "https://apps.apple.com/us/app/orlando-mco/id939593688", type: .Flights, driverCategory: DriverCategory(id: .Flights, page: .weather, image: "")),
            
            //MARK: - Hotels
            Driver(id: "Orlando World Center Marriott", directLink: "mwcwayfinding", safeLink: "https://apps.apple.com/us/app/orlando-world-center-marriott/id1311856070", type: .Hotels, driverCategory: DriverCategory(id: .Hotels, page: .travel, image: "")),
            
           //MARK: - Calendars
            Driver(id: "Visit Orlando", directLink: "visitorlando://", safeLink: "https://apps.apple.com/us/app/visit-orlando-app/id1122736071", type: .Calendars, driverCategory: DriverCategory(id: .Calendars, page: .travel, image: "")),
            Driver(id: "TouringPlans", directLink: "", safeLink: "https://apps.apple.com/us/app/touringplans-lines-universal-orlando-unofficial/id825306842", type: .Calendars, driverCategory: DriverCategory(id: .Calendars, page: .travel, image: "")),
            Driver(id: "Magic Guide for Disney World", directLink: "versaedge.wdwmagicguide://", safeLink: "https://apps.apple.com/us/app/magic-guide-for-disney-world/id365817457", type: .Calendars, driverCategory: DriverCategory(id: .Calendars, page: .travel, image: "")),
           //MARK: - Restaraunts
           // Driver(id: "Hand-Picked", directLink: "scoutmobhp://", safeLink: "https://apps.apple.com/us/app/hand-picked-atlanta/id346700012", type: .Restaraunts, driverCategory: DriverCategory(id: .Restaraunts, page: .travel, image: "")),
            //MARK: - Mobile Food
           // Driver(id: "Uber eats", directLink: "ubereats", safeLink: "https://apps.apple.com/us/app/uber-eats-food-delivery/id1058959277", type: .MobileFood, driverCategory: DriverCategory(id: .MobileFood, page: .travel, image: "")),
            
            //MARK: - TicketedEvents
            
            Driver(id: "MLB At Bat", directLink: "mlbatbat://", safeLink: "https://apps.apple.com/us/app/mlb-at-bat/id493619333", type: .TicketedEvents, driverCategory: DriverCategory(id: .TicketedEvents, page: .travel, image: "")),
            Driver(id: "NHL", directLink: "NHL://", safeLink: "https://apps.apple.com/us/app/nhl/id465092669", type: .TicketedEvents, driverCategory: DriverCategory(id: .TicketedEvents, page: .travel, image: "")),
            Driver(id: "Orlando Magic", directLink: "orlandomagic://", safeLink: "https://apps.apple.com/us/app/orlando-magic-mobile/id731742786", type: .TicketedEvents, driverCategory: DriverCategory(id: .TicketedEvents, page: .travel, image: "")),
            Driver(id: "Orlando Theme Park", directLink: "uct-orlando://", safeLink: "https://apps.apple.com/us/app/orlando-theme-park-wait-times/id385303379", type: .TicketedEvents, driverCategory: DriverCategory(id: .TicketedEvents, page: .travel, image: "")),
            Driver(id: "Universal Orlando Resort", directLink: "universal-orlando://", safeLink: "https://apps.apple.com/us/app/universal-orlando-resort/id878217080", type: .TicketedEvents, driverCategory: DriverCategory(id: .TicketedEvents, page: .travel, image: "")),
            Driver(id: "SeaWorld Discovery Guide", directLink: "", safeLink: "https://apps.apple.com/us/app/seaworld/id393159815", type: .TicketedEvents, driverCategory: DriverCategory(id: .TicketedEvents, page: .travel, image: "")),
            
            
            
            

            
            //MARK: - Reviews
           // Driver(id: "Trip Advisor", directLink: "tripadvisor", safeLink: "https://apps.apple.com/us/app/tripadvisor-hotels-restaurants/id284876795", type: .Reviews, driverCategory: DriverCategory(id: .Reviews, page: .travel, image: "")),
            
            //MARK: - Coffee
          //  Driver(id: "Starbucks", directLink: "starbucks", safeLink: "https://apps.apple.com/us/app/starbucks/id331177714", type: .Coffee, driverCategory: DriverCategory(id: .Coffee, page: .travel, image: "")),
            //MARK: - Translator
           // Driver(id: "Speak & Translate", directLink: "translatorfreenew", safeLink: "", type: .Translator, driverCategory: DriverCategory(id: .Translator, page: .travel, image: "")),
        ])
    }
}
