//
//  DriverList.swift
//  Mate
//
//  Created by Landon Brambert on 9/16/19.
//  Copyright © 2019 Onyx Theaters.tv, INC. All rights reserved.
//

import Foundation
import HomeKit

class Miami: Drivers {
    init() {
        super.init(drivers: [
            //MARK: - Public Transportation
            Driver(id: "EASY Pay Miami", directLink: "miamieasypay://", safeLink: "https://apps.apple.com/us/app/easy-pay-miami/id1402202692", type: .PublicTransportation, driverCategory: DriverCategory(id: .PublicTransportation, page: .travel, image: "")),
            Driver(id: "Miami-Dade Transit", directLink: "", safeLink: "https://apps.apple.com/us/app/miami-dade-transit/id1146998811", type: .PublicTransportation, driverCategory: DriverCategory(id: .PublicTransportation, page: .travel, image: "")),
            Driver(id: "MDT Tracker", directLink: "MiamiDadeTransitTracker://", safeLink: "https://apps.apple.com/us/app/mdt-tracker/id464426407", type: .PublicTransportation, driverCategory: DriverCategory(id: .PublicTransportation, page: .travel, image: "")),
            Driver(id: "Transit Tracker", directLink: "tt.mia://", safeLink: "https://apps.apple.com/us/app/transit-tracker-miami-dade/id688264353", type: .PublicTransportation, driverCategory: DriverCategory(id: .PublicTransportation, page: .travel, image: "")),
            
            
            
            
            
            
            
            //MARK: - RideShare
           
            Driver(id: "Citi Bike", directLink: "", safeLink: "https://apps.apple.com/us/app/citi-bike-miami/id398956248", type: .RideShare, driverCategory: DriverCategory(id: .RideShare, page: .travel, image: "")),
            
            //MARK: - Flights
        //    Driver(id: "Orlando MCO", directLink: "", safeLink: "https://apps.apple.com/us/app/orlando-mco/id939593688", type: .Flights, driverCategory: DriverCategory(id: .Flights, page: .weather, image: "")),
            
            //MARK: - Hotels
         //   Driver(id: "Orlando World Center Marriott", directLink: "mwcwayfinding", safeLink: "https://apps.apple.com/us/app/orlando-world-center-marriott/id1311856070", type: .Hotels, driverCategory: DriverCategory(id: .Hotels, page: .travel, image: "")),
            
           //MARK: - Calendars
            Driver(id: "Miami Travel Guide", directLink: "fb2050571868576748://", safeLink: "https://apps.apple.com/us/app/miami-travel-guide/id386132391", type: .Calendars, driverCategory: DriverCategory(id: .Calendars, page: .travel, image: "")),
            
           //MARK: - Restaraunts
           // Driver(id: "Hand-Picked", directLink: "scoutmobhp://", safeLink: "https://apps.apple.com/us/app/hand-picked-atlanta/id346700012", type: .Restaraunts, driverCategory: DriverCategory(id: .Restaraunts, page: .travel, image: "")),
            //MARK: - Mobile Food
           // Driver(id: "Uber eats", directLink: "ubereats", safeLink: "https://apps.apple.com/us/app/uber-eats-food-delivery/id1058959277", type: .MobileFood, driverCategory: DriverCategory(id: .MobileFood, page: .travel, image: "")),
            
            //MARK: - TicketedEvents
            
            Driver(id: "MLB At Bat", directLink: "mlbatbat://", safeLink: "https://apps.apple.com/us/app/mlb-at-bat/id493619333", type: .TicketedEvents, driverCategory: DriverCategory(id: .TicketedEvents, page: .travel, image: "")),
            Driver(id: "NHL", directLink: "NHL://", safeLink: "https://apps.apple.com/us/app/nhl/id465092669", type: .TicketedEvents, driverCategory: DriverCategory(id: .TicketedEvents, page: .travel, image: "")),
            Driver(id: "MMM Live", directLink: "MMMLive://", safeLink: "https://apps.apple.com/us/app/mmm-live/id1322879763", type: .TicketedEvents, driverCategory: DriverCategory(id: .TicketedEvents, page: .travel, image: "")),
            Driver(id: "Miami Hurricanes", directLink: "fb373810602704245bbbbcace://", safeLink: "https://apps.apple.com/us/app/miami-hurricanes/id555140249", type: .TicketedEvents, driverCategory: DriverCategory(id: .TicketedEvents, page: .travel, image: "")),
            Driver(id: "Miami Dolphins", directLink: "dolphinsmobile://", safeLink: "https://apps.apple.com/us/app/miami-dolphins/id563617073", type: .TicketedEvents, driverCategory: DriverCategory(id: .TicketedEvents, page: .travel, image: "")),
            Driver(id: "Miami HEAT", directLink: "miamiheat://", safeLink: "https://apps.apple.com/us/app/miami-heat-mobile/id497407923", type: .TicketedEvents, driverCategory: DriverCategory(id: .TicketedEvents, page: .travel, image: "")),
            Driver(id: "Pérez Art Museum", directLink: "fb723533051015974museum165://", safeLink: "https://apps.apple.com/us/app/pérez-art-museum-miami-pamm/id1215768854", type: .TicketedEvents, driverCategory: DriverCategory(id: .TicketedEvents, page: .travel, image: "")),
            Driver(id: "Museum of Discovery & Science", directLink: "", safeLink: "https://apps.apple.com/us/app/museum-of-discovery-science/id1276163216", type: .TicketedEvents, driverCategory: DriverCategory(id: .TicketedEvents, page: .travel, image: "")),
            Driver(id: "Exhibitionary", directLink: "fb1708622379405758://", safeLink: "https://apps.apple.com/us/app/exhibitionary-art-guide/id1105802393", type: .TicketedEvents, driverCategory: DriverCategory(id: .TicketedEvents, page: .travel, image: "")),
            Driver(id: "Zoo Miami", directLink: "", safeLink: "https://apps.apple.com/us/app/zoo-miami/id1169063160", type: .TicketedEvents, driverCategory: DriverCategory(id: .TicketedEvents, page: .travel, image: "")),
            
            
            
            
            
            

            
            //MARK: - Reviews
           // Driver(id: "Trip Advisor", directLink: "tripadvisor", safeLink: "https://apps.apple.com/us/app/tripadvisor-hotels-restaurants/id284876795", type: .Reviews, driverCategory: DriverCategory(id: .Reviews, page: .travel, image: "")),
            
            //MARK: - Coffee
          //  Driver(id: "Starbucks", directLink: "starbucks", safeLink: "https://apps.apple.com/us/app/starbucks/id331177714", type: .Coffee, driverCategory: DriverCategory(id: .Coffee, page: .travel, image: "")),
            //MARK: - Translator
           // Driver(id: "Speak & Translate", directLink: "translatorfreenew", safeLink: "", type: .Translator, driverCategory: DriverCategory(id: .Translator, page: .travel, image: "")),
        ])
    }
}
